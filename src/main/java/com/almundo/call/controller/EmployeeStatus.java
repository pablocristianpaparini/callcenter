package com.almundo.call.controller;

/** No usada por ahora, con esto podriamos manejar diversos estados de los asistentes admeas de los
 * que estan.
 * 
 * @author pablo.paparini */
public enum EmployeeStatus {

  OnCall, Available

}
